package model;

public class CashCard {
	private double balance ;
	
	public CashCard(double amount){
		this.balance = amount;
	}
	
	public void withdraw (double amount){
		this.balance -= amount;
	}
	
	public void deposit (double amount){
		this.balance += amount;
	}
	
	public double checkMoney (){
		return this.balance;
	}
	

}
