package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.CashCard;
import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		CashCard card1 = new CashCard(0);
		card1.deposit(200);
		frame.setResult("Deposit : "+card1.checkMoney()+"Baht");
		card1.checkMoney();
		frame.extendResult("CheckMoney : "+card1.checkMoney()+"Baht");
		card1.withdraw(100);
		frame.extendResult("Withdraw : "+card1.checkMoney()+"Baht");
		card1.checkMoney();
		frame.extendResult("CheckMoney : "+card1.toString()+"Baht");
	}

	ActionListener list;
	SoftwareFrame frame;
}
